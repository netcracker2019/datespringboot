package com.netcracker.date.config;

import com.netcracker.date.domain.Person;
import com.netcracker.date.repository.PersonRepository;
import com.netcracker.date.utils.Roles;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@EnableOAuth2Sso
@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .mvcMatchers("/").permitAll()
                //.anyRequest().authenticated()
                .and()
                .csrf().disable();
    }

    @Bean
    public PrincipalExtractor principalExtractor(PersonRepository personRepository){
        return map -> {
            String id = (String)map.get("sub");
            Person user = personRepository.findByName(id); // Add Further
            if(user == null){
                Person person = new Person();
                person.setName(id); // setLogin
                person.setEmail((String) map.get("email"));
                person.setGender((String)map.get("gender"));
                person.setRole(Roles.User);
                user = person;
            }
            return personRepository.save(user);
        };
    }
}