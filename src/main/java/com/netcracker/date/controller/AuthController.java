package com.netcracker.date.controller;
import com.netcracker.date.domain.Person;
import com.netcracker.date.service.*;
import com.netcracker.date.utils.Roles;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@Log4j2
public class AuthController {
    private PersonService personService;
    private InterestService interestService;
    private MeetingService meetingService;
    private PlaceService placeService;
    private RatingService ratingService;

    @Autowired
    public AuthController(PersonService personService,
                          InterestService interestService,
                          MeetingService meetingService,
                          PlaceService placeService,
                          RatingService ratingService) {
        this.meetingService = meetingService;
        this.personService = personService;
        this.interestService = interestService;
        this.placeService = placeService;
        this.ratingService = ratingService;
//        personService.save(new Person("admin","admin","admin","admin", Roles.Admin));
//        personService.save(new Person("123","123","123","123", Roles.User));
//        personService.save(new Person("new","123","9198759","mailru", Roles.User));
//        personService.save(new Person("Daddy","123","222213","yandex", Roles.User));


    }

    @RequestMapping(value = "/allUsers", method = RequestMethod.GET)
    public List<Person> getAllUsers(){
        return this.personService.findAll();
    }

    @RequestMapping(value = "/registerPerson", method = RequestMethod.POST)
    public ResponseEntity registerUser(@RequestBody Person person, @RequestParam("email") String email){
        log.info("Registration for person " + person.getName());
        log.info("Get email " + email);
        if(personService.isNameAvailable(person.getName())){
            personService.registrate(person, email );
            return ResponseEntity.ok(HttpStatus.OK);
        }
        log.warn("This nickname is already existed");
        return ResponseEntity.ok(HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public ResponseEntity authUser(@RequestBody Person person){
        log.info("Auth of person " + person.getName());
        return ResponseEntity.ok(this.personService.checkUser(person));
    }

    @RequestMapping(value = "/isAdmin", method = RequestMethod.POST)
    public Boolean isPersonAdmin(@RequestBody String s){
        log.info("Nick is "+ s + " here");
        Roles roles = this.personService.findByName(s).getRole();
        Boolean result = roles.equals(Roles.Admin);
        return result;
    }

    @RequestMapping(value = "/editPerson", method = RequestMethod.POST)
    public void editPerson(@RequestBody Person person){
        this.personService.editPerson(person);
    }

    @RequestMapping(value = "/deletePerson", method = RequestMethod.POST)
    public void deletePerson(@RequestBody Person person){
        log.info("Delete person");
        this.personService.deletePerson(person.getName());
    }

    @RequestMapping(value = "/editProfile", method = RequestMethod.POST)
    public void editProfile(@RequestBody Person person){
        log.info("Edit of profile " + person.getName());
        this.personService.editProfile(person);
    }

    @RequestMapping(value = "/addCompanion", method = RequestMethod.POST)
    public ResponseEntity addCompanion(@RequestBody String email){
        log.info("Checking of companion, email = " + email);
        return ResponseEntity.ok(this.personService.isEmailAdded(email));
    }

    @RequestMapping(value = "/createMeeting", method = RequestMethod.POST)
    public ResponseEntity createMeeting(@RequestBody String[] companions, @RequestParam("name") String name){
        log.info("Adding meeting for " + name + ", companions = " + companions);
        return ResponseEntity.ok(meetingService.createMeeting(name, companions));
    }

    @RequestMapping(value = "/saveInterestsFromUser", method = RequestMethod.POST)
    public ResponseEntity saveInterestsFromUser(@RequestBody Object[] userInterests){
        log.info("Saving person's interests " + userInterests[0]);
        return ResponseEntity.ok(interestService.saveUserInterests(userInterests));
    }

    @RequestMapping(value = "/getMeetings", method = RequestMethod.POST)
    public ResponseEntity getMeetings(@RequestBody String name){
        log.info("Getting all meetings for " + name);
        return ResponseEntity.ok(meetingService.getMeetingsAndInterestsByName(name));
    }

    @RequestMapping(value = "/getAllInterests", method = RequestMethod.GET)
    public ResponseEntity getAllInterests(){
        log.info("Getting All Interests from Data base");
        return ResponseEntity.ok(this.interestService.findAll());
    }

    @RequestMapping(value = "/mergeInterests", method = RequestMethod.POST)
    public ResponseEntity mergeInterests(@RequestBody String name){
        log.info("Merging interests for person {}", name);
        return ResponseEntity.ok(this.interestService.mergeInterests(name));
    }

    @RequestMapping(value = "/savePlacesToVote", method = RequestMethod.POST)
    public ResponseEntity savePlacesToVote(@RequestBody Object object){
        log.info("Saving places from ya-map for {}", object);
        return ResponseEntity.ok(this.placeService.savePlacesToVote(object));
    }

    @RequestMapping(value = "/showPlaces", method = RequestMethod.POST)
    public  ResponseEntity showPlaces(@RequestBody String name){
        log.info("Checking choosing places for {}", name);
        return ResponseEntity.ok(this.placeService.showPlacesToVote(name));
    }

    @RequestMapping(value = "/voteForMeeting", method = RequestMethod.POST)
    public ResponseEntity voteForMeeting(@RequestBody Object votedPlaces,
                                         @RequestParam("name") String name,
                                         @RequestParam("meetingId") Long meetingId
                                         ){
        log.info("Voting for meeting {} by Person {}", meetingId, name);
        return ResponseEntity.ok(this.ratingService
                        .saveVotedPlacesForMeeting(votedPlaces, meetingId, name));
    }

    @RequestMapping(value = "/finishVoting", method = RequestMethod.POST)
    public ResponseEntity finishVoting(@RequestBody String name){
        log.info("Analyzing if any meeting needs to finish it");
        return ResponseEntity.ok(this.ratingService.finishVoting(name));
    }

}
