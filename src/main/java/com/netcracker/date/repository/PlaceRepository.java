package com.netcracker.date.repository;

import com.netcracker.date.domain.Place;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaceRepository extends JpaRepository<Place,Long> {
    Place getById(Long id);

    Place findByNameAndAddress(String name, String address);
}
