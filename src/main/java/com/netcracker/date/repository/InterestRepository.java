package com.netcracker.date.repository;

import com.netcracker.date.domain.Interest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InterestRepository extends JpaRepository<Interest,Long> {

    Interest getById(int id);
}
