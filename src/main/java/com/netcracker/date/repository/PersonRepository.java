package com.netcracker.date.repository;

import com.netcracker.date.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
    Person findByName(String name);
    Person findByEmail(String email);

    @Transactional
    @Modifying
    @Query("update Person p set " +
            "p.name = :name, " +
            "p.passwords = :password, " +
            "p.phone = :phone, " +
            "p.email = :email_upd," +
            "p.role = 1 " +
            "where p.email = :email_par")
    void updateByEmail(@Param("name") String name,
                       @Param("password") String password,
                       @Param("phone") String phone,
                       @Param("email_upd") String email,
                       @Param("email_par") String emailGet);


    void deleteByName(String name);

    Person getById(Long id);
}
