package com.netcracker.date.repository;

import com.netcracker.date.domain.PersonInMeeting;
import com.netcracker.date.domain.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Set;

public interface RatingRepository extends JpaRepository<Rating,Long> {

    Set<Rating> findByOfPersonInMeeting(PersonInMeeting pim);

}
