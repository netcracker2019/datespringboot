package com.netcracker.date.repository;

import com.netcracker.date.domain.Meeting;
import com.netcracker.date.domain.Person;
import com.netcracker.date.domain.PersonInMeeting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.cdi.Eager;
import org.springframework.stereotype.Repository;

import java.util.LinkedHashSet;
import java.util.List;

@Repository
public interface PersonInMeetingRepository extends JpaRepository<PersonInMeeting, Long> {

    //Get all Meetings where exact person exists
    List<PersonInMeeting> getPersonInMeetingByPerson(Person person);

    //Get all Persons where exact meeting exists
    List<PersonInMeeting> getPersonInMeetingByMeeting(Meeting meeting);

    List<PersonInMeeting> findPersonInMeetingsByPerson(Person person);

    //Get all interests by Person and by Meeting
    PersonInMeeting getPersonInMeetingByPersonAndMeeting(Person person, Meeting meeting);

}
