package com.netcracker.date.service;

import com.netcracker.date.domain.Meeting;
import com.netcracker.date.domain.Person;
import com.netcracker.date.domain.PersonInMeeting;
import com.netcracker.date.domain.Place;
import com.netcracker.date.repository.MeetingRepository;
import com.netcracker.date.repository.PersonInMeetingRepository;
import com.netcracker.date.repository.PersonRepository;
import com.netcracker.date.repository.PlaceRepository;
import lombok.extern.log4j.Log4j2;
import org.codehaus.jackson.map.util.JSONPObject;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Array;
import java.util.*;

@Log4j2
@Service
public class PlaceService {
    private PlaceRepository placeRepository;
    private PersonInMeetingRepository personInMeetingRepository;
    private MeetingRepository meetingRepository;
    private PersonRepository personRepository;

    @Autowired
    public PlaceService(PlaceRepository placeRepository,
                        PersonInMeetingRepository personInMeetingRepository,
                        MeetingRepository meetingRepository,
                        PersonRepository personRepository) {
        this.placeRepository = placeRepository;
        this.personInMeetingRepository = personInMeetingRepository;
        this.meetingRepository = meetingRepository;
        this.personRepository = personRepository;
    }

    public void save(Place place) {
        this.placeRepository.save(place);
    }

    public Place findById(Long id ){
        return this.placeRepository.getById(id);
    }

    public HttpStatus savePlacesToVote(Object object){
        log.info("************************");
        log.info("В PlaceService");
        log.info(object);
        Map<String, Object> newObject= (Map<String, Object>) object;
        String name = (String) newObject.get("name");
        Long idOfMeeting = Long.parseLong((String) newObject.get("idOfMeeting"));
        ArrayList<Map<String, String>> places= (ArrayList<Map<String, String>>) newObject.get("places");
        log.info(newObject);
        log.info(name);
        log.info(idOfMeeting);
        log.info(places);


        Person curPerson = personRepository.findByName(name);
        Meeting curMeeting = meetingRepository.getById(idOfMeeting);
        PersonInMeeting personChoseInThisMeeting = personInMeetingRepository
                .getPersonInMeetingByPersonAndMeeting(curPerson, curMeeting);

        //Getting places because of lazily initialization
        Set<Place> placesSet = curMeeting.getPlaces();

        //At first -- adding to Place.table
        //At second -- connect with Meeting.table
        Set<Place> placesToAddToCurMeeting= new HashSet<>();
        for (Map<String, String> place: places) {
            Place curPlaceToAdd= placeRepository.
                    findByNameAndAddress(place.get("name"), place.get("address"));
            log.info(curPlaceToAdd);
            if (curPlaceToAdd == null){
                curPlaceToAdd = new Place(place.get("name"),
                    place.get("address"),
                    place.get("url"),
                        place.get("hours"));
                placeRepository.save(curPlaceToAdd);
            }
            placesToAddToCurMeeting.add(curPlaceToAdd);
            log.info("Adding place to cur meeting");
            //Just for now
        }
        //Connecting to places
        curMeeting.addPlaces(placesToAddToCurMeeting);
        meetingRepository.save(curMeeting);

        //Choosing of some person
        personChoseInThisMeeting.setDidChoose(true);
        personInMeetingRepository.save(personChoseInThisMeeting);

        return HttpStatus.CREATED;
    }

    public Map<Long, Set<Place>> showPlacesToVote(String name){
        Person curPerson = personRepository.findByName(name);
        List<PersonInMeeting> personInMeetings = personInMeetingRepository
                .getPersonInMeetingByPerson(curPerson);
        //Objects to return
        Map<Long, Set<Place>> meetingPlaces = new HashMap<>();

        //All meetings
        for (PersonInMeeting pim: personInMeetings) {
            log.info("current PIM is {}", pim);
            Meeting curMeeting = pim.getMeeting();
            List<PersonInMeeting> companionMeetings = personInMeetingRepository
                    .getPersonInMeetingByMeeting(curMeeting);
            int lengthOfChosen = companionMeetings.size();
            log.info("All companions {} in Meeting {}",
                    lengthOfChosen, curMeeting.getId());
            int currentSizeOfChosen = 0;
            //Current meeting's companions
            for (PersonInMeeting companion: companionMeetings) {
                log.info("Current companion is {}", companion);
                if (companion.isDidChoose())
                    currentSizeOfChosen++;
            }
            log.info("True length= {}, and cur length= {}",
                    lengthOfChosen, currentSizeOfChosen);
            if (currentSizeOfChosen == lengthOfChosen){
                Set<Place> curPlaces = new HashSet<>();
                curPlaces.addAll(curMeeting.getPlaces());
                log.info("New places have been added {} of meeting {}",
                        curPlaces, curMeeting.getId());
                meetingPlaces.put(curMeeting.getId(), curPlaces);
            }
            log.info("Cur meetingPlaces (returning obj) {}", meetingPlaces);
        }

        return meetingPlaces;
    }
}
