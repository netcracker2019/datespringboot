package com.netcracker.date.service;

import com.netcracker.date.domain.*;
import com.netcracker.date.repository.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

@Log4j2
@Service
public class RatingService {
    private PlaceRepository placeRepository;
    private PersonInMeetingRepository personInMeetingRepository;
    private MeetingRepository meetingRepository;
    private PersonRepository personRepository;
    private RatingRepository ratingRepository;

    @Autowired
    public RatingService(PlaceRepository placeRepository,
                         PersonInMeetingRepository personInMeetingRepository,
                         MeetingRepository meetingRepository,
                         PersonRepository personRepository,
                         RatingRepository ratingRepository) {
        this.placeRepository = placeRepository;
        this.personInMeetingRepository = personInMeetingRepository;
        this.meetingRepository = meetingRepository;
        this.personRepository = personRepository;
        this.ratingRepository = ratingRepository;
    }

    public HttpStatus saveVotedPlacesForMeeting(Object votedPlaces,
                                                Long meetingId, String name){
        ArrayList<ArrayList<Object>> newVotedPlaces = (ArrayList<ArrayList<Object>>) votedPlaces;

        Person curPerson = personRepository.findByName(name);
        Meeting meeting = meetingRepository.getById(meetingId);

        PersonInMeeting curPersonInCurMeeting = personInMeetingRepository
                .getPersonInMeetingByPersonAndMeeting(curPerson, meeting);

        log.info("Before saving Voted places");
        log.info("By {} in meeting {}", name, meetingId);
        log.info("votedPlaces are {}", votedPlaces);

        //Set for saving at PersonInMeeting
        Set<Rating> ratingSet = new HashSet<>();

        for(ArrayList<Object> obj: newVotedPlaces){

            log.info("Place {}",obj.get(0).getClass().getName());
            HashMap<String, Object> placeObj = (HashMap<String, Object>)obj.get(0);
            Long idOfCurPlace =  new Long((int) placeObj.get("id"));
            log.info("Place id {}", idOfCurPlace);
            int curRating = (int) obj.get(1);
            log.info("Rating {}", obj.get(1));

            Place curPlace = placeRepository.getById(idOfCurPlace);

            //Creating rating object and save it in db

            Rating ratingForCupPIMandCurPlace = new Rating(curPersonInCurMeeting,
                    curPlace, curRating);
            ratingSet.add(ratingForCupPIMandCurPlace);
            ratingRepository.save(ratingForCupPIMandCurPlace);
        }

        //Voting of person in meeting
        curPersonInCurMeeting.setDidVote(true);
        personInMeetingRepository.save(curPersonInCurMeeting);
        return HttpStatus.CREATED;
    }

    public Map<Long, Place> finishVoting(String name){
        Person curPerson = personRepository.findByName(name);

        List<PersonInMeeting> meetingsOfPerson = personInMeetingRepository
                .getPersonInMeetingByPerson(curPerson);

        //Returning Map
        Map<Long, Place> returningFinishingPlaces = new HashMap<>();
        //For counting votes of
        int counterOfVoting;
        Map<Long, Integer> pointsForPlaces = new HashMap<>();
        for(PersonInMeeting personInMeeting: meetingsOfPerson){
            Meeting curMeeting = personInMeeting.getMeeting();


            List<PersonInMeeting> allMeetingsAndPersons = personInMeetingRepository
                    .getPersonInMeetingByMeeting(curMeeting);
            //Preparing for counting votes
            counterOfVoting = 0;

            for(PersonInMeeting pim: allMeetingsAndPersons){
                log.info("Finishing voting for person {} in meeting {}",
                        pim.getPerson().getName(), pim.getMeeting().getId());

                if(!pim.isDidVote()){
                    break;
                }
                //If this person voted
                else{
                    counterOfVoting++;
                    Set<Rating> ratingByPIM = ratingRepository.findByOfPersonInMeeting(pim); // pim.getRatings()
                    //For all his rating of places
                    for(Rating curRating: ratingByPIM){
                        log.info("Rating {}", curRating);
                        Long idOfPlaceForMeeting = curRating.getToPlace().getId();
                        pointsForPlaces.merge(idOfPlaceForMeeting,
                                curRating.getRateToPlaceInMeeting(), (a,b) -> a+b); //Check this
                    }
                }
            }
            //Voting has been finished
            if(counterOfVoting == allMeetingsAndPersons.size()){
                log.info("Finishing voting for meeintg {}", curMeeting.getId());
                log.info("Points array {}", pointsForPlaces);
                Place curFinishingPlace = countingVotes(pointsForPlaces);
                //Saving Returning place
                returningFinishingPlaces.put(curMeeting.getId(), curFinishingPlace);
            }
            //Clear the temp array of votes
            pointsForPlaces.clear();
        }

        return returningFinishingPlaces;
    }

    public Place countingVotes(Map<Long, Integer> points){
        Long idOfFinishPlace = Collections.max(points.entrySet(), Map.Entry.comparingByValue()).getKey();
        log.info(Collections.max(points.entrySet(), Map.Entry.comparingByValue()).getKey());

        Place place = placeRepository.getById(idOfFinishPlace);

        return place;
    }


}