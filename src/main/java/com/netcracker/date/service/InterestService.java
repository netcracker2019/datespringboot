package com.netcracker.date.service;

import com.netcracker.date.domain.Interest;
import com.netcracker.date.domain.Meeting;
import com.netcracker.date.domain.Person;
import com.netcracker.date.domain.PersonInMeeting;
import com.netcracker.date.repository.InterestRepository;
import com.netcracker.date.repository.MeetingRepository;
import com.netcracker.date.repository.PersonInMeetingRepository;
import com.netcracker.date.repository.PersonRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Service
public class InterestService {
    private InterestRepository interestRepository;
    private PersonRepository personRepository;
    private MeetingRepository meetingRepository;
    private PersonInMeetingRepository personInMeetingRepository;

    @Autowired
    public InterestService(InterestRepository interestRepository,
                           PersonRepository personRepository,
                           MeetingRepository meetingRepository,
                           PersonInMeetingRepository personInMeetingRepository) {
        this.interestRepository = interestRepository;
        this.personRepository = personRepository;
        this.meetingRepository = meetingRepository;
        this.personInMeetingRepository = personInMeetingRepository;
    }

    public void save(Interest interest){
        this.interestRepository.save(interest);
    }

    public List<Interest> findAll(){
        return this.interestRepository.findAll();
    }

    public Interest getInterestByIdForUser(int id){
        return this.interestRepository.getById(id);
    }

    public HttpStatus getUserInterests(){

        return HttpStatus.FOUND;
    }

    public HttpStatus saveUserInterests(Object[] userInterests){
        String name = (String) userInterests[0];
        Person person = personRepository.findByName(name);
        Long idOfMeeting = Long.parseLong((String) userInterests[1]);

        Meeting meeting = meetingRepository.getById(idOfMeeting);

        PersonInMeeting personInMeeting = personInMeetingRepository
                .getPersonInMeetingByPersonAndMeeting(person, meeting);

        ArrayList<Map> interests = (ArrayList<Map>) userInterests[2];
        log.info("User " + name);
        log.info("Meeting {}", meeting);
        log.info("Interests: " + interests);

        log.info("PIMmeeting {}", personInMeeting);
        for (int i = 0; i < interests.size(); i++) {
            log.info(interests.get(i).get("id"));
            personInMeeting.addInterest(interestRepository.getById((int) interests.get(i).get("id")));
        }
        log.info("After cycle adding {}", personInMeeting);
        log.info("GETTING INTERESTS {}", personInMeeting.getInterests());

        personInMeetingRepository.save(personInMeeting);

        return HttpStatus.CREATED;
    }

    public ArrayList<Object> mergeSameInterests(ArrayList<Set<Interest>> interests,
                                                ArrayList<Set<String>> categories) {

        String[] ints;
        Set<Interest> resultInterestsForSearching = new HashSet<>();
        Set<String> resultCategoriesForSearching = new HashSet<>();

        Set<String> categoryByAlg = new HashSet<>();

        ArrayList<Integer> countOfCategories; // 0==Food, 1==Culture, 2==Dance, 3==Music, 4==Activities, 5==Sports, 6==Games

        //Comparing by all interests
        if(interests.size() > 1 ){
            log.info("More then one user add interests");
            Set<Interest> curInterestIntersect = new HashSet<>();
            curInterestIntersect.addAll(interests.get(0));
            //Beginning form the second set
            log.info("Current state of curInterestIntersect");
            log.info(curInterestIntersect);
            for (int i = 1; i < interests.size(); i++) { //Finding Intersections
                curInterestIntersect.retainAll(interests.get(i));
                log.info("State of curInterestIntersect after retainAll with "
                        + i + " set of interests");
                log.info(curInterestIntersect);
            }
            //If there is no one intersection in companions interests
            if(curInterestIntersect.isEmpty()){
                log.info("NO intersects in INTERESTS!");
                //Comparing by categories
                log.info("Comparing by categories");
                Set<String> curCategoryIntersect = new HashSet<>(); //Linked ?
                curCategoryIntersect.addAll(categories.get(0));
                log.info("Current state of curCategoryIntersect{}", curCategoryIntersect);
                for (int i = 1; i < categories.size(); i++) { // Beginning from the second element
                    curCategoryIntersect.retainAll(categories.get(i));
                    log.info("Current state of curCategoryIntersect after retainAll with "
                            + i + " set of Categories");
                    log.info(curCategoryIntersect);
                }
                if(curCategoryIntersect.isEmpty()){
                    log.info("NO intersects NOR INTERESTS,NOR CATEGORIES");
                    //Some algorithm
                    log.info("Working of some Algorithm");

                    Map<String, Integer> beforeSorting = countOfCategories(categories);
                    log.info("Counting categories {}", beforeSorting);
                    //Sorting of map by streams
                    Map<String, Integer> afterSorting = new LinkedHashMap<>();
                    afterSorting = beforeSorting.entrySet().stream()
                            .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
                    log.info("After sorting {}", afterSorting);

                    //Checking if 1 maximum
                    ArrayList<Integer> forCheckingMax =  new ArrayList<>(afterSorting.values());

                    if (forCheckingMax.get(0) > forCheckingMax.get(1) ){
                        String maxCat = afterSorting.keySet().iterator().next();
                        log.info("THere are maximum {}", maxCat);

                        log.info("There is max in Cats");
                        resultCategoriesForSearching.add(maxCat);
                    }
                    else{
                        int counter = 0;
                        for(Map.Entry<String, Integer> cat: afterSorting.entrySet()){
                            log.info("There will be only 2 most wanted categories");
                            if (counter > 2){
                                break;
                            }
                            log.info("Key in cats = {}", cat.getKey());
                            resultCategoriesForSearching.add(cat.getKey());
                            counter++;
                        }
                    }


                }
                else{
                    log.info("THERE ARE intersects in CATEGORIES");
                    log.info("curCategoryIntersect: {}", curCategoryIntersect);
                    resultCategoriesForSearching.addAll(curCategoryIntersect);
                }
            }
            else{
                log.info("THERE ARE intersects in CATEGORIES");
                log.info("curInterestIntersect: {}", curInterestIntersect);
                resultInterestsForSearching.addAll(curInterestIntersect);
            }
        }
        //If only 1 person has added
        else{
            log.info("Only 1 person add interests!");
            resultInterestsForSearching.addAll(interests.get(0));
        }

        log.info("Algorithm has ended with InterestsForSearching: {}", resultInterestsForSearching);
        log.info("Algorithm has ended with CategoriesForSearching: {}", resultCategoriesForSearching);

        //Adding some results
        ArrayList<Object> resultingIntersections = new ArrayList<>();
        resultingIntersections.add(resultInterestsForSearching);
        resultingIntersections.add(resultCategoriesForSearching);

        return resultingIntersections;
    }

    public Map<Long, Object> mergeInterests(String name){
        ArrayList<Set<Interest>> interests = new ArrayList<>();
        ArrayList<Set<String>> categories = new ArrayList<>();

        //for every meeting
        Map<Long, Object> interestsAndCategoriesAfterMerging = new HashMap<>();//Linked ??

        Person curPerson = personRepository.findByName(name);
        List<PersonInMeeting> meetingsOfPerson = personInMeetingRepository
                .getPersonInMeetingByPerson(curPerson);

        //For every meeting to merge interests
        for(PersonInMeeting meeting: meetingsOfPerson){
            log.info("Current meeting is {}", meeting.getMeeting().getId());
            Meeting curMeeting = meeting.getMeeting();
            List<PersonInMeeting> personsOfMeeting = personInMeetingRepository
                    .getPersonInMeetingByMeeting(curMeeting);
            //Every person interests to analyze
            for(PersonInMeeting pim : personsOfMeeting){
                //****Attention
                Set<Interest> interestsOfPersonInMeeting = pim.getInterests();
                //If interests exist
                if(!interestsOfPersonInMeeting.isEmpty()) {
                    log.info("Interests exist for {} in meeting {}",
                            pim.getPerson().getName(), meeting.getMeeting().getId());
                    interests.add(interestsOfPersonInMeeting);
                    //Adding categories of exact person in exact meeting
                    Set<String> personCategories = new HashSet<>(); //Нужен Linked ??
                    for (Interest interest : interestsOfPersonInMeeting) {
                        personCategories.add(interest.getCategory());
                    }
                    //Adding all categories to ArrayList to merge in algorithmic function
                    categories.add(personCategories);
                }
                else{
                    log.info("There are NOT interests for {} in meeting {}",
                            pim.getPerson().getName(),pim.getMeeting().getId());
                }
            }
            log.info("Array of interests {} and of categories {} before algorithm of Merging",
                    interests, categories);
            if (!interests.isEmpty()) {
                log.info("There will be ALGORITHM of merging");
                ArrayList<Object> algResultForSearching = new ArrayList<>();
                algResultForSearching = mergeSameInterests(interests, categories);

                log.info("Result from mergeSameInterests algorithm: {}", algResultForSearching);
                //Adding Intersection for Interests and Categories for Exact meeting
                interestsAndCategoriesAfterMerging.put(curMeeting.getId(), algResultForSearching);
            }
            else{
                log.info("No algorithm because of NOT existing interests");
            }
            interests.clear();
            categories.clear();
        }

        return interestsAndCategoriesAfterMerging;
    }

    public Map<String,Integer> countOfCategories(ArrayList<Set<String>> cats){
        Map<String, Integer> catsCounterMap = new HashMap<>();
        for(Set<String> catsFriend: cats){
            for(String cat: catsFriend){
                switch (cat) {
                    case  ("Food"):
                        catsCounterMap.merge("Food", 1, (a,b) -> a+b);
                        break;
                    case ("Culture"):
                        catsCounterMap.merge("Culture", 1, (a,b) -> a+b);
                        break;
                    case ("Dance"):
                        catsCounterMap.merge("Dance", 1, (a,b) -> a+b);
                        break;
                    case ("Music"):
                        catsCounterMap.merge("Music", 1, (a,b) -> a+b);
                        break;
                    case ("Activities"):
                        catsCounterMap.merge("Activities", 1, (a,b) -> a+b);
                        break;
                    case ("Sports"):
                        catsCounterMap.merge("Sports", 1, (a,b) -> a+b);
                        break;
                    case ("Games"):
                        catsCounterMap.merge("Games", 1, (a,b) -> a+b);
                        break;
                }
            }
        }
        return catsCounterMap;
    }

}
