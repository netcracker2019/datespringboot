package com.netcracker.date.service;

import com.netcracker.date.domain.Interest;
import com.netcracker.date.domain.Meeting;
import com.netcracker.date.domain.Person;
import com.netcracker.date.domain.PersonInMeeting;
import com.netcracker.date.repository.MeetingRepository;
import com.netcracker.date.repository.PersonInMeetingRepository;
import com.netcracker.date.repository.PersonRepository;
import com.netcracker.date.utils.Roles;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

@Log4j2
@Service
public class MeetingService {
    private MeetingRepository meetingRepository;
    private PersonRepository personRepository;
    private PersonInMeetingRepository personInMeetingRepository;
    private EmailServiceImpl emailService;

    @Autowired
    public MeetingService(MeetingRepository meetingRepository,
                          PersonRepository personRepository,
                          PersonInMeetingRepository personInMeetingRepository,
                          EmailServiceImpl emailService) {

        this.meetingRepository = meetingRepository;
        this.personRepository = personRepository;
        this.personInMeetingRepository = personInMeetingRepository;
        this.emailService = emailService;
    }

    public void save(Meeting meeting) {
        this.meetingRepository.save(meeting);
    }

    public List<Meeting> findAll() {
        return this.meetingRepository.findAll();
    }

    public HttpStatus createMeeting(String name, String[] companions) {
        Person creatingPerson = personRepository.findByName(name);

        ArrayList<String> emailsToSend = new ArrayList<>();

        Meeting meeting = new Meeting("Meeting");
        meetingRepository.save(meeting);
        log.info("Saving of MEETING");

        //Adding creating person to meeting
        PersonInMeeting creatingPIM = new PersonInMeeting(creatingPerson, meeting);
        personInMeetingRepository.save(creatingPIM);
        log.info("newPIM {}", creatingPIM);
        //For companions
        for (String comp : companions) {
            log.info("Element " + comp);
            Person checkPerson = personRepository.findByEmail(comp);
            if (checkPerson == null) { //If person DOESN'T EXISTS
                log.info("For user " + comp + " is necessary to register");

                emailsToSend.add(comp);

                Person curPerson = new Person("", "", "", comp, Roles.User);
                personRepository.save(curPerson);
                //Adding in 2 Tables
                PersonInMeeting newPIM = new PersonInMeeting(curPerson, meeting);
                log.info("newPIM {}", newPIM);
                personInMeetingRepository.save(newPIM);
            } else {
                //Adding in 2 tables
                PersonInMeeting newPIM = new PersonInMeeting(checkPerson, meeting);
                log.info("newPIM {}", newPIM);
                personInMeetingRepository.save(newPIM);
                log.info("Adding user to meeting without any mails");
            }
        }
        //Sending Notifications on emails
        emailService.sendNotificationsOnCreationMeeting(emailsToSend);
        log.info("Adding all user to DB to connect them with meeting");
        return HttpStatus.CREATED;
    }

    public Map<String, Object> getMeetingsAndInterestsByName(String name) {
        Person curPerson = personRepository.findByName(name);

        //ToSaveALL
        Map<Long, ArrayList<Interest>> interestingInMeeting = new HashMap<>(); //OWN interests in different meetings
        Map<Long, List<PersonInMeeting>> interestsOfFriendsInMeetings = new HashMap<>(); //Friend's interests in diff meetings

        List<PersonInMeeting> pimMeetings = personInMeetingRepository.getPersonInMeetingByPerson(curPerson);

        log.info("=============");
        log.info("curPerson {}", curPerson );
        log.info("pimMeetings {}", pimMeetings);
        log.info("=============");

        List<List<PersonInMeeting>> personInMeetings = new ArrayList<>();

        for (PersonInMeeting meeting : pimMeetings) {
            List<PersonInMeeting> pimPersonsOfMeeting = personInMeetingRepository
                    .getPersonInMeetingByMeeting(meeting.getMeeting());
            personInMeetings.add(pimPersonsOfMeeting);
            interestsOfFriendsInMeetings.put(meeting.getMeeting().getId(), pimPersonsOfMeeting);
        }

        log.info(personInMeetings);

        Map<String, Object> response = new HashMap<>();

        response.put("ownInterests", pimMeetings);
        response.put("friendsInterests", interestsOfFriendsInMeetings);

        return response;
    }

}