package com.netcracker.date.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import java.util.ArrayList;

@Log4j2
@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    public JavaMailSender emailSender;
    public void sendSimpleMessage(String to, String subject, String text) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(to);
            message.setSubject(subject);
            message.setText(text);
            emailSender.send(message);
        } catch (MailException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void prepareAndSend(String recipient, String message) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom("datesearcher0@gmail.com");
            messageHelper.setTo(recipient);
            messageHelper.setSubject("Invitation to registration on DateSearch");
            messageHelper.setText("Hello! Register by this url: " + message
                    + '\n' + "We are DareSearcher2019");
        };
        try {
            emailSender.send(messagePreparator);
        } catch (MailException e) {
            log.info("Mail EXCEPTION");
        }
    }


    public void sendNotificationsOnCreationMeeting(ArrayList<String> emails) {
        for (String email : emails) {
            log.info(email);
            String url = "http://localhost:4200/registration?email=" + email;
            prepareAndSend(email, url);
            log.info("Sending new email " + email);
        }
    }


}
