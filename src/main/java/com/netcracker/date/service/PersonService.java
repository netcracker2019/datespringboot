package com.netcracker.date.service;

import com.netcracker.date.domain.Person;
import com.netcracker.date.repository.PersonInMeetingRepository;
import com.netcracker.date.repository.PersonRepository;
import com.netcracker.date.utils.Roles;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

@Log4j2
@Service
public class PersonService {
    private PersonRepository personRepository;

    private MeetingService meetingService;

    private EmailServiceImpl emailService;

    private PersonInMeetingRepository personInMeetingRepository;
    private InterestService interestService;

    @Autowired
    public PersonService(PersonRepository personRepository,
                         EmailServiceImpl emailService,
                         MeetingService meetingService,
                         PersonInMeetingRepository personInMeetingRepository,
                         InterestService interestService) {
        this.personRepository = personRepository;

        this.emailService = emailService;

        this.meetingService = meetingService;
        this.personInMeetingRepository = personInMeetingRepository;
        this.interestService = interestService;


    }

    public void save(Person person) {
        this.personRepository.save(person);
    }

    public Person findByName(String name) {
        return this.personRepository.findByName(name);
    }

    public void deleteById(Long id) {
        this.personRepository.deleteById(id);
    }

    public void deleteByName(String name) {
        this.personRepository.deleteByName(name);
    }

    public List<Person> findAll() {
        return this.personRepository.findAll();
    }

    public Boolean isNameAvailable(String name) {
        List<Person> list = new ArrayList<>(personRepository.findAll());
        for (Person p : list) {
            if (p.getName() == null || p.getName().isEmpty()) {
                continue;
            }
            if (p.getName().equals(name)) {
                return false;
            }
        }
        return true;
    }

    public HttpStatus checkUser(Person person) {
        HttpStatus returnValue = HttpStatus.NOT_FOUND;
        List<Person> list = new ArrayList<>();
        list.addAll(personRepository.findAll());
        for (Person p : list) {
            if (p.getName() == null || p.getName().isEmpty()) {
                continue;
            }
            if (p.getEmail() == null || p.getEmail().isEmpty()) {
                continue;
            }
            if (p.getName().equals(person.getName()) || p.getEmail().equals(person.getName())) {
                returnValue = HttpStatus.EXPECTATION_FAILED;
                if (p.getPasswords().equals(person.getPasswords())) {
                    returnValue = HttpStatus.FOUND;
                }
            }
        }
        return returnValue;
    }

    public void editPerson(Person person) {
        Person person1 = this.personRepository.findByName(person.getName());
        person1.setEmail(person.getEmail());
        person1.setRole(person.getRole());
        person1.setPhone(person.getPhone());
        this.personRepository.save(person1);
    }

    public void deletePerson(String name) {
        Person person = this.personRepository.findByName(name);
        this.personRepository.delete(person);
    }

    public void editProfile(Person person) {
        log.info("New email: " + person.getEmail());
        log.info("New phone: " + person.getPhone());
    }

    public HttpStatus isEmailAdded(String email) {
        log.info("***********************");
        log.info("CHECK " + email);
        log.info("***********************");
        log.info("USER");
        if (personRepository.findByEmail(email) == null) {
            log.info(email + " doesnt existed in db");
            return HttpStatus.NOT_FOUND;
        } else {
            log.info("SUCCESS");
            log.info(email);
            return HttpStatus.FOUND;
        }
    }

    public void registrate(Person person, String email) {
        log.info("EMAIL===== " + email);
        if (!email.isEmpty()) {
            log.info("Not EMPTY");
            this.personRepository.updateByEmail(person.getName(), person.getPasswords(),
                    person.getPhone(), person.getEmail(), email); //email -- parameter
        } else {
            save(new Person(person.getName(), person.getPasswords(), person.getPhone(), person.getEmail(), Roles.User));
        }
    }
}