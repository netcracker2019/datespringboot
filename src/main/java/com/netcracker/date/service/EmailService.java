package com.netcracker.date.service;

public interface EmailService {

    void sendSimpleMessage(String to,
                           String subject,
                           String text);

    void prepareAndSend(String recipient, String message);
}
