package com.netcracker.date.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.netcracker.date.utils.Roles;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@EqualsAndHashCode(exclude={"meetingsByPerson"})
@ToString(exclude = {"meetingsByPerson"})
public class Person {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String passwords;
    private String phone;
    private String email;
    private Roles role;
    private String name;
    private String gender;
    private String city;

    @OneToMany
    @JsonIgnore
    private List<PersonInMeeting> meetingsByPerson = new ArrayList<>();


    public Person(String name, String passwords, String phone, String email, Roles role) {
        this.name = name;
        this.passwords = passwords;
        this.phone = phone;
        this.email = email;
        this.role = role;
    }

    public Person(){}

    public void addMeeting(PersonInMeeting PersonInMeeting) {
        meetingsByPerson.add(PersonInMeeting);
    }

}
