package com.netcracker.date.domain;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(exclude={"interests"})
public class PersonInMeeting {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToOne
    private Person person;

    @ManyToOne
    private Meeting meeting;

    @ManyToMany(fetch=FetchType.EAGER)
    private Set<Interest> interests;

    //Did person choose the places
    private boolean didChoose;

    //Did person vote for places
    private boolean didVote;

    public PersonInMeeting(Person person, Meeting meeting){
        this.person = person;
        this.meeting = meeting;
    }

    public void addInterest(Interest interest){
        this.interests.add(interest);
    }

    //Chose places
    public void choose(){
        this.didChoose = true;
    }

    //Voted for places
    public void vote(){
        this.didVote = true;
    }

}
