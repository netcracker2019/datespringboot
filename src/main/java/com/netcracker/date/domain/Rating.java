package com.netcracker.date.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(exclude={"ofPersonInMeeting", "toPlace"})
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int rateToPlaceInMeeting;

    @ManyToOne
    private PersonInMeeting ofPersonInMeeting;

    @ManyToOne
    private Place toPlace;


    public Rating(PersonInMeeting pim, Place place, int rating){
        this.ofPersonInMeeting = pim;
        this.toPlace = place;
        this.rateToPlaceInMeeting = rating;
    }
}
