package com.netcracker.date.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;


@Entity
@Data
@NoArgsConstructor
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"name", "address"})
})
@EqualsAndHashCode(exclude={"meetings"})
@ToString(exclude = {"meetings"})
public class Place {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String address;
    private String url;
    private String hours;

    @ManyToMany(mappedBy = "places", fetch = FetchType.LAZY)
    private Set<Meeting> meetings;

    public Place(String name, String address, String url, String hours) {
        this.name = name;
        this.address = address;
        this.url = url;
        this.hours = hours;
    }
}
