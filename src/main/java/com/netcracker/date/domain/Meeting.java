package com.netcracker.date.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(exclude={"allPersonsByMeeting", "places"})
@ToString(exclude = {"allPersonsByMeeting", "places"})
public class Meeting {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Date date;
    private Time time;

    @OneToMany
    @JsonIgnore
    private List<PersonInMeeting> allPersonsByMeeting = new ArrayList<>();

    @ManyToMany(fetch=FetchType.EAGER)
    @JsonIgnore
    private Set<Place> places;

    public Meeting(String name) {
        this.name = name;
    }

    public void addPerson(PersonInMeeting personInMeeting) {
        allPersonsByMeeting.add(personInMeeting);
    }

//    @Transactional
    public void addPlace(Place place){
        places.add(place);
    }

//    @Transactional
    public void addPlaces(Set<Place> places){ //Set
        this.places.addAll(places);
    }

}
