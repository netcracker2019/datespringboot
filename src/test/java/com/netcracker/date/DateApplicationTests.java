package com.netcracker.date;

import com.netcracker.date.controller.AuthController;
import com.netcracker.date.domain.Person;
import com.netcracker.date.service.PersonService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DateApplicationTests {

    @Autowired
    private PersonService personService;

    @Test
    public void contextLoads() {
    }
//
//    @Test
//    public void testCheckUser() {
//        HttpStatus httpStatus= this.personService.checkUser(new Person("admin","admin",null,null,null));
//        assertThat(httpStatus).isEqualTo(HttpStatus.FOUND);
//    }
//
//    @Test
//    public void testIsNameAvailable(){
//        Boolean isAvailable = this.personService.isNameAvailable("123");
//        assertThat(isAvailable).isFalse();
//    }

}
